from django.shortcuts import render
from django.views import generic
from core.models import Equipos

# Create your views here.
class inicio(generic.View):
    template_name = "home/inicio.html"
    context={}
    def get(self, request, *args, **kwargs):
        equipo =  Equipos.objects.all()
        self.context = {
            "Equipos": equipo
        }
        return render(request, self.template_name,self.context)
    