from django import forms
from .models import *

##estadios

class estadiosforms(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Equipos": forms.Select(attrs={"class":"form-select form-control"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        labels = {
            "Descripcion" : "Descripción", 
            }



class actulizarestadiosforms(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Equipos": forms.Select(attrs={"class":"form-select form-control"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        labels = {
            "Descripcion" : "Descripción", 
            }

##equpos

class equipoforms(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Ciudad": forms.Select(attrs={"class":"form-select form-control"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        labels = {
            "Descripcion" : "Descripción", 
            }
        
class actualizarequiposforms(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Ciudad": forms.Select(attrs={"class":"form-select form-control"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        labels = {
            "Descripcion" : "Descripción", 
            }

##ciudad

class ciudadesforms(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Estado": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del estado"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        labels = {
            "Descripcion" : "Descripción", 
            }

class actualizarciudadesforms(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = "__all__"
        widgets = {
            "Nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            "Estado": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del estado"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre de la ciudad"}),
            }
        labels = {
            "Descripcion" : "Descripción", 
            }