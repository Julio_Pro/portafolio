# Generated by Django 4.2.7 on 2023-11-06 10:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ciudades',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(default='Minneapolis', max_length=35)),
                ('Estado', models.CharField(default='Minnesota', max_length=15)),
                ('Descripcion', models.CharField(default='Esta ciudad estadounidense es aficionada a los Minnesota', max_length=555)),
                ('fecha', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Equipos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(default='Viking', max_length=35)),
                ('Descripcion', models.CharField(default='Este equipo se formo en 1990', max_length=555)),
                ('fecha', models.DateField()),
                ('Ciudad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.ciudades')),
            ],
        ),
        migrations.CreateModel(
            name='Estadios',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(default='Bank Stadium', max_length=35)),
                ('Descripcion', models.CharField(default='El estadio abrio sus puertas en 2001', max_length=555)),
                ('fecha', models.DateField()),
                ('Equipos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.equipos')),
            ],
        ),
    ]
